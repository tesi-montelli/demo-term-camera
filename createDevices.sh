#!/bin/bash
NUM_THERM=$1
NUM_CAMERA=$2

#fare build delle immagini per i device
docker build -t fiware-device/thermometer ./devices/thermometerMQTT &>/dev/null

docker build -t fiware-device/camera ./devices/cameraMQTT &>/dev/null

sleep 1m

#dare il launch, per questa demo si può partire da ON
for ((i=1; i<=${NUM_THERM}; i++))
do
  echo "therm ${i}"
  devices/thermometerMQTT/launcher.sh ${i} on 1000 0 &
done


for ((i=1; i<=${NUM_CAMERA}; i++))
do
  echo "camera ${i}"
  devices/cameraMQTT/launcher.sh ${i} on 1000 &
done
