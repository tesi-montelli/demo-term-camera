#!/bin/bash

docker-compose up --build &>/dev/null &

sleep 20s

curl -iX POST \
  'http://localhost:4041/iot/services' \
  -H 'Content-Type: application/json' \
  -H 'fiware-service: openiot' \
  -H 'fiware-servicepath: /' \
  -d '{
 "services": [
   {
     "apikey":      "4jggokgpepnvsb2uv4s40d59ov",
     "cbroker":     "http://orion:1026",
     "entity_type": "Thing",
     "resource":    ""
   }
 ]
}' #&>/dev/null


curl -iX POST \
  --url 'http://localhost:8082/v2/subscriptions' \
  --header 'Content-Type: application/json' \
  --header 'fiware-service: openiot' \
  --header 'fiware-servicepath: /' \
  --data '{
  "description": "Notify me when any Thermometer changes state",
  "subject": {
  "entities": [{"idPattern": ".*","type": "Thermometer"}],
  "condition": {
    "attrs": ["Status", "Temperature", "Timestamp"]
  }
  },
  "notification": {
  "http": {
    "url": "http://node:3000/api/fiware/notification/thermometer"
  },
  "attrsFormat" : "keyValues"
  }
}' #&>/dev/null


curl -iX POST \
  --url 'http://localhost:8082/v2/subscriptions' \
  --header 'Content-Type: application/json' \
  --header 'fiware-service: openiot' \
  --header 'fiware-servicepath: /' \
  --data '{
  "description": "Notify me when any Camera changes state",
  "subject": {
  "entities": [{"idPattern": ".*","type": "Camera"}],
  "condition": {
    "attrs": ["Status", "Image", "Timestamp"]
  }
  },
  "notification": {
  "http": {
    "url": "http://node:3000/api/fiware/notification/camera"
  },
  "attrsFormat" : "keyValues"
  }
}' #&>/dev/null
