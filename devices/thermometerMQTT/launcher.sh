#!/bin/bash

NUM=$1
STATUS=$2
TIME=$3
PAYLOAD=$4

FIWARE_IOTA="127.0.0.1"
IOTA_PORT="4041"
NETWORK="demo-term-camera_default"

curl \
    --max-time 10 \
    --connect-timeout 2 \
    --retry 5 \
    --retry-delay 0 \
    --retry-max-time 40 \
  -iX POST \
  "http:/${FIWARE_IOTA}:${IOTA_PORT}/iot/devices" \
  -H 'Content-Type: application/json' \
  -H 'fiware-service: openiot' \
  -H 'fiware-servicepath: /' \
  -d '{
  "devices": [{
    "device_id":   "'"thermometer$NUM"'",
    "entity_name": "'"urn:ngsi-ld:thermometer:$NUM"'",
    "entity_type": "Thermometer",
    "transport": "MQTT",
    "commands": [
      { "name": "on", "type": "command" },
      { "name": "off", "type": "command" }
    ],
    "attributes": [
      { "object_id": "t", "name": "Temperature", "type": "Float" },
      { "object_id": "s", "name": "Status", "type": "Boolean" },
      { "object_id": "time", "name": "Time", "type": "Integer" },
      { "object_id": "p", "name": "Payload", "type": "String"}
    ]
  }]
}'

echo "post post"
#pass NUM to docker as env variable
docker run \
  --env ID=${NUM} \
  --network ${NETWORK} \
  --env STATUS=${STATUS} \
  --env TIME=${TIME} \
  --env PAYLOAD_KB=${PAYLOAD} \
  --name term${NUM} \
fiware-device/thermometer
