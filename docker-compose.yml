# WARNING: Do not deploy this tutorial configuration directly to a production environment
#
# The tutorial docker-compose files have not been written for production deployment and will not
# scale. A proper architecture has been sacrificed to keep the narrative focused on the learning
# goals, they are just used to deploy everything onto a single Docker machine. All FIWARE components
# are running at full debug and extra ports have been exposed to allow for direct calls to services.
# They also contain various obvious security flaws - passwords in plain text, no load balancing,
# no use of HTTPS and so on.
#
# This is all to avoid the need of multiple machines, generating certificates, encrypting secrets
# and so on, purely so that a single docker-compose file can be read as an example to build on,
# not use directly.
#
# When deploying to a production environment, please refer to the Helm Repository
# for FIWARE Components in order to scale up to a proper architecture:
#
# see: https://github.com/FIWARE/helm-charts/
#
version: "3.5"
services:
  # Orion is the context broker
    orion:
        image: fiware/orion:${ORION_VERSION}
        hostname: orion
        container_name: fiware-orion
        depends_on:
            - mongo-db-orion
        networks:
            - default
        expose:
            - "${ORION_PORT}"
        ports:
            - "${ORION_PORT_EXT}:${ORION_PORT}" # localhost:1026
        ##modificato sia per indicare di usare cors e per indicare il nome del nuovo DB
        command: -corsOrigin __ALL -dbhost mongo-db-orion -logLevel DEBUG
        healthcheck:
            test: curl --fail -s http://orion:${ORION_PORT}/version || exit 1
            interval: 5s

    #iot agent
    iot-agent:
        image: fiware/iotagent-json:${JSON_VERSION}
        hostname: iot-agent
        container_name: fiware-iot-agent
        depends_on:
            - mongo-db-orion
            - mosquitto
        networks:
            - default
        expose:
            - "${IOTA_NORTH_PORT}"
        ports:
            - "${IOTA_NORTH_PORT}:${IOTA_NORTH_PORT}" # localhost:4041
        environment:
          - IOTA_CB_HOST=orion # name of the context broker to update context
          - IOTA_CB_PORT=${ORION_PORT} # port the context broker listens on to update context
          - IOTA_NORTH_PORT=${IOTA_NORTH_PORT}
          - IOTA_REGISTRY_TYPE=mongo-db-orion #Whether to hold IoT device info in memory or in a database
          - IOTA_LOG_LEVEL=DEBUG # The log level of the IoT Agent
          - IOTA_TIMESTAMP=true # Supply timestamp information with each measurement
          - IOTA_CB_NGSI_VERSION=v2 # use NGSIv2 when sending updates for active attributes
          - IOTA_AUTOCAST=true # Ensure Ultralight number values are read as numbers not strings
            ##Modificato hostnmae di mongodb
          - IOTA_MONGO_HOST=mongo-db-orion # The host name of MongoDB
          - IOTA_MONGO_PORT=${MONGO_DB_FIWARE_PORT_INT} # The port mongoDB is listening on
          - IOTA_MONGO_DB=iotagentjs # The name of the database used in mongoDB
          - IOTA_MQTT_HOST=mosquitto # The host name of the MQTT Broker
          # Use internal port, not the exposed one
          - IOTA_MQTT_PORT=${MOSQUITTO_PORT_INT} # The port the MQTT Broker is listening on to receive topics
          - IOTA_DEFAULT_RESOURCE= # Default is blank. I'm using MQTT so I don't need a resource
          - IOTA_PROVIDER_URL=http://iot-agent:${IOTA_NORTH_PORT}
        healthcheck:
            interval: 5s

    # Database (ORION)
    mongo-db-orion:
        image: mongo:${MONGO_DB_VERSION}
        hostname: mongo-db-orion
        container_name: mongo-db-orion
        expose:
            - "${MONGO_DB_FIWARE_PORT_INT}"
        ports:
            - "${MONGO_DB_FIWARE_PORT_EXT}:${MONGO_DB_FIWARE_PORT_INT}" # localhost:27017
        networks:
            - default
        volumes:
            - mongo-db-orion:/data
        healthcheck:
            test: |
                host=`hostname --ip-address || echo '127.0.0.1'`;
                mongo --quiet $host/test --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)' && echo 0 || echo 1
            interval: 5s


    # MQTT Broker
    mosquitto:
        image: eclipse-mosquitto:1.6.14
        hostname: mosquitto
        container_name: mosquitto
        expose:
            - "${MOSQUITTO_PORT_EXT}"
            - "9001"
        ports:
            - "${MOSQUITTO_PORT_EXT}:${MOSQUITTO_PORT_INT}"
            - "9001:9001"
        volumes:
            - ./fiware/mosquitto/mosquitto.conf:/mosquitto/config/mosquitto.conf
        networks:
            - default
        environment:
            - "PUID=${MOSQUITTO_PORT_EXT}"
            - "PGID=${MOSQUITTO_PORT_EXT}"

    #Web server
    node:
        restart: always
        #Usa il dockerfile dentro la cartella "webserver"
        build: ./webserver
        ports:
            - ${WEB_SERVER_PORT_EXT}:3000
        networks:
            - default
        volumes:
            - ./webserver:/code
        depends_on:
            - mongo-db-web
            - orion
            - iot-agent

    #MongoDB (WebServer)
    mongo-db-web:
        image: mongo:${MONGO_DB_VERSION}
        hostname: mongo-db-web
        container_name: mongo-db-web
        command: mongod --port ${MONGO_DB_NODE_PORT_INT}
        networks:
            - default
        ports:
            - ${MONGO_DB_NODE_PORT_EXT}:${MONGO_DB_NODE_PORT_INT}
        volumes:
            - mongo-db-web:/webserver/data/db



networks:
  default_net:
    ipam:
      config:
        - subnet: 172.18.1.0/24

volumes:
  mongo-db-orion: ~
  mongo-db-web:
