# Setup and launch

The first time you execute this demo  launch `docker-compose up --build` to ensure that fiware is correctly deployed. After this first check use `docker-compose down` to stop the running containers.

To run the experiment, execute `./launch.sh X Y` where X ad Y are, respectively, the number of desired Themometers and Cameras
The experiment can be halted by launching `stopExperiment.sh`

# Dashboard
A dashboard is accessibile at `127.0.0.1:8080`